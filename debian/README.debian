Init scripts
============

Both systemd and SysV init scripts are provided, but the SysV scripts aren't as
well-tested as the systemd ones. Bug reports and patches are welcome to improve
the LXD experience on non-systemd installs.

When installing LXD on a non-systemd host, you must ensure that a cgroup v2
hierarchy is mounted prior to starting LXD. One possible way to do this is to
add a line like the following to your /etc/fstab:

    cgroup2   /sys/fs/cgroup   cgroup2   rw,nosuid,nodev,noexec   0 0

Storage backends
================

LXD supports several storage backends. When installing, LXD will suggest the
necessary packages to enable all storage backends, but in brief:

  * btrfs requires the btrfs-progs package
  * ceph/cephfs require the ceph-common package
  * lvm requires the lvm2 package
  * zfs requires the zfsutils-linux package

After installing one or more of those additional packages, be sure to restart
the LXD service so it picks up the additional storage backend(s).

Virtual machines
================

LXD optionally can create virtual machine instances utilizing QEMU. To enable
this capability, on the host system install the desired qemu-system-<arch>
package(s) and the lxd-agent package. Then, restart the LXD service. You will
now be able to create virtual machine instances by passing the `--vm` flag in
your creation command.

Known issues
============

  * Running LXD and Docker on the same host can cause connectivity issues. A
common reason for these issues is that Docker sets the FORWARD policy to DROP,
which prevents LXD from forwarding traffic and thus causes the instances to lose
network connectivity. There are two different ways you can fix this:

    - As outlined in bug #865975, message 91, you can add `net.ipv4.ip_forward=1`
      to /etc/sysctl.conf which will create a FORWARD policy that docker can
      use. Docker then won't set the FORWARD chain to DROP when it starts up.

    - Alternately, you can use the following command to explicitly allow
      network traffic from your network bridge to your external network
      interface:
      `iptables -I DOCKER-USER -i <network_bridge> -o <external_interface> -j ACCEPT`
      (from the upstream LXD documentation at
       https://linuxcontainers.org/lxd/docs/master/howto/network_bridge_firewalld/)

  * If the apparmor package is not installed on the host system, containers
will fail to start unless their configuration is modified to include
`lxc.apparmor.profile=unconfined`; this has been reported upstream at
https://github.com/lxc/lxc/issues/4150.
